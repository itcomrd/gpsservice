package com.itcomrd.gpsservice;

import com.itcomrd.gpsservice.GPSServiceMain;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.widget.Toast;

public class GPSServiceActivity extends Activity {
	private final static String[] PERMISSIONS = {
			Manifest.permission.ACCESS_FINE_LOCATION};
	private final static int REQUEST_PERMISSIONS = 1;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		// サービスクラスを指定したインテントの作成
		if(checkPermissions()) {
			Intent intent = new Intent(this, GPSServiceMain.class);
			// サービスの起動
			startService(intent);
			finish();
		}
   	 }
	//ユーザーの利用許可のチェック
	private boolean checkPermissions() {
		//未許可
		boolean ret = isGranted();
		if (!ret) {
			//許可ダイアログの表示
			ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSIONS);
		}
		return ret;
	}

	//ユーザーの利用許可が済かどうかの取得
	private boolean isGranted() {
		for (int i  = 0; i < PERMISSIONS.length; i++) {
			if (PermissionChecker.checkSelfPermission(this, PERMISSIONS[i]) != PackageManager.PERMISSION_GRANTED) {
				return false;
			}
		}
		return true;
	}

	//許可ダイアログ選択時に呼ばれる
	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] results) {
		if (requestCode == REQUEST_PERMISSIONS) {
			//未許可
			if (!isGranted()) {
				toast(getString(R.string.no_permission));
			} else {
				Intent intent = new Intent(this, GPSServiceMain.class);
				// サービスの起動
				startService(intent);
				finish();
			}
		} else {
			super.onRequestPermissionsResult(requestCode, permissions, results);
		}
	}
	//トーストの表示
	private void toast(String text) {
		Toast.makeText(this, text, Toast.LENGTH_LONG).show();
	}
}